import { useStrict } from 'mobx';
import DeviceStore from '../store/Devices';

useStrict(true);

const deviceStore = new DeviceStore();

export default function() {
  return {
    deviceStore: deviceStore,
  };
}
