import * as React from 'react';
import { Container, Header, Title, Content, Body, Right, Icon, Button } from 'native-base';
import DeviceStore from '../store/Devices';
import DeviceItem from '../components/DeviceItem';
import { observer, inject } from 'mobx-react';

interface Props {
    deviceStore?: DeviceStore;
    navigation?: any;
}

@inject('deviceStore')
@observer
export default class MainPage extends React.Component<Props> {
    render() {
        return (
            <Container>
                <Header>
                    <Body>
                        <Title>Wireless Manager</Title>
                    </Body>
                    <Right>
                        <Button onPress={() => this.props.navigation.navigate('AddNewDevice')} transparent>
                            <Icon name='add' />
                        </Button>
                    </Right>
                </Header>
                <Content>
                    {this.props.deviceStore.devices.map(device => (
                        <DeviceItem key={device.ipAddress} ip={device.ipAddress} />
                    ))}
                </Content>
            </Container>
        );
    }
}