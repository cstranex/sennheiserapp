import * as React from 'react';
import { Container, Header, Title, Text, Content, Body, Left, Icon, Button, Subtitle, Input, Form, Item } from 'native-base';
import { Toast, Separator, ListItem, CheckBox, Label } from 'native-base';
import DeviceStore from '../store/Devices';
import { observer, inject } from 'mobx-react';
import { observable, action, computed } from 'mobx';
import { NetworkInfo } from 'react-native-network-info';

const IP_ADDRESS_REGEX = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;

interface Props {
    deviceStore?: DeviceStore;
    navigation?: any;
}

interface State {
    ipAddress: string;
}

@inject('deviceStore')
@observer
export default class AddNewDevice extends React.Component<Props, State> {

    @observable checkboxStates: string[] = [];

    constructor(props: Props) {
        super(props);
        this.state = {
            ipAddress: ''
        }
    }

    componentDidMount() {
        new Promise((resolve, reject) => {
            NetworkInfo.getIPV4Broadcast((address: string) => {
                console.log("Broadcast address is: " + address);
                resolve(address);
            });
        }).then((broadcastAddress: string) => {
            this.props.deviceStore.discoverDevices(broadcastAddress);
        });
    }

    addDevice(ipAddress?: string, stay?: boolean) {

        if (!ipAddress && this.state.ipAddress === '') {
            for (let ip of this.checkboxStates) {
                this.addDevice(ip, true);
            }
            this.props.navigation.goBack();
            return;
        }

        if (!IP_ADDRESS_REGEX.test(ipAddress)) {
            Toast.show({
                text: 'IP Address is invalid',
                position: 'bottom',
                type: 'danger'
            });
            return;
        }

        console.log("Going to add device " + ipAddress);

        let device = this.props.deviceStore.addDevice(ipAddress);

        if (device) {
            device.connect();
            if (!stay) {
                this.props.navigation.goBack();
            }
        } else {
            Toast.show({
                text: 'Device has been already added',
                position: 'bottom',
                type: 'danger'
            });
        }
    }

    @action.bound
    toggleCheckbox(ipAddress: string) {
        if (this.checkboxStates.indexOf(ipAddress) !== -1) {
            this.checkboxStates.splice(this.checkboxStates.indexOf(ipAddress), 1);
        } else {
            this.checkboxStates.push(ipAddress);
        }
    }

    @computed get discoveredDevices() {
        const devices = [];
        this.props.deviceStore.discovered.forEach((name, ipAddress) => 
            devices.push({
                name: name,
                ipAddress: ipAddress
            })
        );

        return devices;
    }

    render() {
        return (
            <Container>
                <Header>
                    <Left>
                        <Button onPress={() => this.props.navigation.goBack()} transparent>
                            <Icon name='arrow-back' />
                        </Button>
                    </Left>
                    <Body>
                        <Title>Wireless Manager</Title>
                        <Subtitle>Add New Device</Subtitle>
                    </Body>
                </Header>
                <Content>
                    <Form>
                        <Item stackedLabel>
                            <Label>IP Address</Label>
                            <Input
                                onChangeText={(e) => this.setState({ipAddress: e})}
                                disabled={this.checkboxStates.length > 0}
                                autoFocus={true}
                                keyboardType='numeric'
                                onSubmitEditing={() => this.addDevice(this.state.ipAddress)}
                                returnKeyType='done'
                            />
                        </Item>
                    </Form>
                    <Separator bordered>
                            <Text>Automatically Discovered</Text>
                        </Separator>
                        { this.discoveredDevices.map(item => (    
                            <ListItem key={item.ipAddress}>
                            <CheckBox
                                checked={this.checkboxStates.indexOf(item.ipAddress) !== -1}
                                onPress={() => this.toggleCheckbox(item.ipAddress)}
                            />
                            <Body>
                                <Text>{item.ipAddress} ({item.name})</Text>
                            </Body>
                            </ListItem>
                        ))}
                    <Button onPress={() => this.addDevice(this.state.ipAddress)} block>
                        <Text>Add</Text>
                    </Button>
                </Content>
            </Container>
        );
    }
}