import * as React from 'react';
import { Container, Header, Content, Tab, Tabs, Text, Form, Label, Input } from 'native-base';
import { Title, Subtitle, Left, Button, Icon, Body, Item, ListItem, Right } from 'native-base';
import { Switch } from 'native-base';
import { Picker } from 'native-base';
import { observer, inject } from 'mobx-react';
import { observable, action, reaction } from 'mobx';
import { withNavigation } from 'react-navigation';
import DeviceStore, { EmEqualizer, SrEqualizer } from '../store/Devices';
import { bankName, formatFrequency } from '../util/frequency';
import { DeviceModel } from '../util/device';
import { BankList } from '../components/BankList';
import FrequencyPicker from '../components/FrequencyPicker';
import DecibelPicker, { EM500_AFOUT, EM2000_AFOUT, SQUELCH } from '../components/NumberPicker';
import HistoryGraph from '../components/HistoryTaker';

/*
    Page that shows more detailed information for device.

    Page is tabbed:
        Settings
        Banks
        History

    Settings:
        Tune frequency, Af level, name, etc, etc.

    Banks:
        View list of banks on the device (Retrieves when page loads)
        Tap a bank/channel to tune
    
    History:
        Show graph of captures of RF level, AF level and battery.

        Starts recording when the FAB on the MainPage is active
        (see components/FabRecord.
*/

interface Props {
    ipAddress: string;
    deviceStore?: DeviceStore;
    navigation: any;
}

class EditableProperties {
    @observable name: string = '';
    @observable frequency: number = 0;
    @observable squelch: number = 0;
    @observable afOut: number = 0;
    @observable equalizer: EmEqualizer | SrEqualizer = EmEqualizer.FLAT;
}

@withNavigation
@inject('deviceStore')
@observer
export default class DeviceView extends React.Component<Props> {

    @observable properties = new EditableProperties();
    @observable ip: string;
    @observable selectedBank: number;

    reactions = [];

    componentWillMount() {
        // Listen to changes to any of the editable properties
        this.ip = this.props.navigation.state.params.ipAddress;
        let device = this.props.deviceStore.byIp(this.ip);

        this.properties.name = device.name;
        this.selectedBank = device.activeBank;

        this.reactions.push(reaction(() => device.name, (name) => {
            this.properties.name = name;
        }));

        this.reactions.push(reaction(() => this.selectedBank, (bank) => {
            device.requestBankList(bank);
        }));

        this.reactions.push(reaction(() => device.equalizer, (eq) => {
            // Only for EM receivers for now
            this.properties.equalizer = eq;
        }));

        // Request the current bank
        device.requestBankList(device.activeBank);
    }

    componentWillUnmount() {
        // Remove the reactions
        this.reactions.map((a) => a());
    }

    updateEqualizer(value: boolean, parameter: string) {
        let low, high = false;
        if (this.properties.equalizer == EmEqualizer.LOW_CUT) {
            low = true;
        } else if (this.properties.equalizer == EmEqualizer.HIGH_BOOST) {
            high = true;
        } else if (this.properties.equalizer == EmEqualizer.LOW_CUT_HIGH_BOOST) {
            low = true;
            high = true;
        }

        if (parameter == 'lowcut') {
            low = value;
        } else {
            high = value;
        }

        let device = this.props.deviceStore.byIp(this.ip);

        if (low && high) {
            device.setEqualizer(EmEqualizer.LOW_CUT_HIGH_BOOST);
        } else if (low) {
            device.setEqualizer(EmEqualizer.LOW_CUT);
        } else if (high) {
            device.setEqualizer(EmEqualizer.HIGH_BOOST);
        } else {
            device.setEqualizer(EmEqualizer.FLAT);
        }
    }

    pickFreq() {
        let device = this.props.deviceStore.byIp(this.ip);
        FrequencyPicker(device.freqRange, device.freqStep, device.frequency).then(
            (value: number) => {
                device.setFrequency(value);
            }
        ).catch(() => {});
    }

    pickAfOut() {
        let device = this.props.deviceStore.byIp(this.ip);
        let outRange = EM500_AFOUT;
        if (device.deviceModel == DeviceModel.EM2000) {
            outRange = EM2000_AFOUT;
        }
        DecibelPicker(outRange, device.afOut, "Select Output Level").then(
            (value: number) => {
                device.setAfOut(value);
            }
        ).catch(() => {});
    }

    pickSquelch() {
        let device = this.props.deviceStore.byIp(this.ip);
        DecibelPicker(SQUELCH, device.squelch, "Select Squelch").then(
            (value: number) => {
                device.setSquelch(value);
            }
        ).catch(() => {});
    }

    render() {
        let device = this.props.deviceStore.byIp(this.ip);

        let devType = '';
        if (device.deviceModel == DeviceModel.EM500) {
            devType = '(EM500)';
        } else if (device.deviceModel == DeviceModel.EM2000) {
            devType = '(EM2000)';
        }

        return (
          <Container>
            <Header>
                <Left>
                    <Button onPress={() => this.props.navigation.goBack()} transparent>
                        <Icon name='arrow-back' />
                    </Button>
                </Left>
                <Body>
                    <Title>Device Overview</Title>
                    <Subtitle>{device.name || "Unknown Device"} {devType}</Subtitle>
                </Body>
            </Header>
            <Tabs initialPage={0}>
              <Tab heading="Attributes">
                <Content>
                    <Form>
                        <Item inlineLabel>
                            <Label>Name</Label>
                            <Input
                                value={this.properties.name}
                                onChangeText={action((e: string) => { this.properties.name = e })}
                                onBlur={() => device.setName(this.properties.name)}
                                disabled={!device.deviceConnected}
                            />
                        </Item>
                        {/* Only for EM receivers */}
                        <ListItem>
                            <Text>Low Cut</Text>
                            <Switch
                                value={this.properties.equalizer == EmEqualizer.LOW_CUT || this.properties.equalizer == EmEqualizer.LOW_CUT_HIGH_BOOST}
                                onValueChange={(v) => this.updateEqualizer(v, 'lowcut')}
                            />
                        </ListItem>
                        <ListItem>
                            <Text>High Boost</Text>
                            <Switch
                                value={this.properties.equalizer == EmEqualizer.HIGH_BOOST || this.properties.equalizer == EmEqualizer.LOW_CUT_HIGH_BOOST}
                                onValueChange={(v) => this.updateEqualizer(v, 'highboost')}
                            />
                        </ListItem>
                        <ListItem onPress={() => this.pickFreq()}>
                            <Text>Frequency</Text>
                            <Right>
                                <Text>{formatFrequency(device.frequency)}</Text>
                            </Right>
                        </ListItem>
                        <ListItem onPress={() => this.pickAfOut()}>
                            <Text>AfOut</Text>
                            <Right>
                                <Text>{device.afOut} dB</Text>
                            </Right>
                        </ListItem>
                        <ListItem onPress={() => this.pickSquelch()}>
                            <Text>Squelch</Text>
                            <Right>
                                <Text>{device.squelch} dB</Text>
                            </Right>
                        </ListItem>
                    </Form>
                </Content>
              </Tab>
              <Tab heading="Presets">
                <Content>
                    <Form>
                        <Picker
                            placeholder="Bank"
                            mode="dropdown"
                            prompt="Select Bank"
                            selectedValue={this.selectedBank}
                            onValueChange={action((n: string) => {this.selectedBank = parseInt(n, 10)})}
                        >
                            {
                                /* Don't show Bank 0 since it is pointless */
                                device.bankList.slice(1).map((bank) => (
                                    <Picker.Item key={bank.bank} label={bankName(bank.bank)} value={bank.bank} />
                                ))
                            }
                        </Picker>
                        <BankList
                            showBank={this.selectedBank}
                            ip={this.ip}
                        />
                    </Form>
                </Content>
              </Tab>
              <Tab heading="History">
                <Content>
                    <HistoryGraph ip={this.ip} />
                </Content>
              </Tab>
            </Tabs>

          </Container>
        );
    }
}