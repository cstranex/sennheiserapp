import React from 'react';
import { BackHandler } from 'react-native';
import { StackNavigator } from 'react-navigation';
import { Root } from 'native-base';

import MainPage from './pages/MainPage';
import AddNewDevice from './pages/AddNewDevice';
import DeviceView from './pages/DeviceView';

const App = StackNavigator(
	{
		MainPage: { screen: MainPage },
		AddNewDevice: { screen: AddNewDevice },
		DeviceView: { screen: DeviceView }
	},
	{
		initialRouteName: 'MainPage',
		headerMode: 'none'
	}
);

BackHandler.addEventListener('hardwareBackPress', function() {
	if (!App.navigation || App.navigation.state.routeName === 'MainPage') {
		BackHandler.exitApp();
	} else {
		App.navigation.goBack();
	}
});

export default () => (
	<Root>
		<App />
	</Root>
);
