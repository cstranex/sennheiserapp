import * as React from 'react';
import { TouchableOpacity } from 'react-native';
import { Toast } from 'native-base';
import { Card, H2, H3, Text, View, Icon } from 'native-base';
import { observer, inject } from 'mobx-react';
import { reaction } from 'mobx';
// import Icon from 'react-native-vector-icons/FontAwesome';
import { Col, Row, Grid } from 'react-native-easy-grid';
import DeviceStore, { Device } from '../store/Devices';
import { AudioLevel, RfLevel } from './Level';
import { formatFrequency, formatPreset } from '../util/frequency';
import { withNavigation } from 'react-navigation';
import BatteryLevel from '../components/BatteryLevel';
import WarningIcon, { WarningType } from '../components/WarningIcon';

const WARNINGS = {
    afPeak: 'AF PEAK',
    lowBattery: 'LOW BATTERY',
    lowRF: 'LOW RF',
    rfMute: 'RF MUTE',
    txMute: 'TX MUTE',
    rxMute: 'RX MUTE'
}

interface Props {
    ip: string;
    navigation?: any;
    deviceStore?: DeviceStore;
}

/* TODO:
    - Make a DeviceItem for EM Receivers and for SR Transmitters
*/

@withNavigation
@inject('deviceStore')
@observer
export default class DeviceItem extends React.Component<Props> {

    warnUser(type, name) {
        Toast.show({
            text: name + ': ' + WARNINGS[type],
            position: 'top',
            type: 'warning',
            buttonText: 'View',
            onClose: () => {
                this.props.navigation.navigate('DeviceView', {ipAddress: this.props.ip});
            }
        });
    }

    componentDidMount() {
        // Warnings reactions
        let device = this.props.deviceStore.deviceByIp[this.props.ip];
        for (let name in device.warnings) {
            reaction(
                () => device.warnings[name],
                state => {
                    if (state) {
                        this.warnUser(name, device.name);
                    }
                }
            );
        }
    }

    render() {
        let device = this.props.deviceStore.deviceByIp[this.props.ip];
        let freq = formatFrequency(device.frequency);
        let preset = formatPreset(device.activeBank, device.activeChannel);

        let warningType = WarningType.NONE;
        if (!device.deviceConnected) {
            warningType = WarningType.DANGER;
        } else if (device.hasWarning) {
            warningType = WarningType.WARNING;
        }

        return (
            <Card
                style={{paddingTop: 2, paddingBottom: 2, paddingLeft: 4, paddingRight: 4}}
            >
                <TouchableOpacity onPress={() => this.props.navigation.navigate('DeviceView', {ipAddress: this.props.ip})}>
                <View>
                <Grid>
                    <Row>
                        <Col size={50}>
                            <H2>{device.name}</H2>
                        </Col>
                        <Col size={19}>
                            <Text style={{textAlign: 'right', paddingTop: 3, paddingRight: 4}} note>{preset}</Text>
                        </Col>
                        <Col size={31}>
                            <H3 style={{textAlign: 'right'}}>{freq}</H3>
                        </Col>
                    </Row>
                    <Row>
                        <Col size={60}>
                            <Row>
                                <Col style={{ width: 20 }}><Text>AF</Text></Col>
                                <Col style={{ paddingTop: 5 }}>
                                    <AudioLevel
                                        value={device.afLevel}
                                        peak={device.afPeakLevel}
                                        disabled={device.afMuted}
                                    /></Col>
                            </Row>
                            <Row>
                                <Col style={{ width: 20 }}><Text>RF</Text></Col>
                                <Col style={{paddingTop: 2.5 }}>
                                    <RfLevel
                                        a={device.rfALevel}
                                        b={device.rfBLevel}
                                        level={device.rfLevel} active={device.rfActiveAntenna}
                                        disabled={device.rfMuted}
                                    />
                                </Col>
                            </Row>
                        </Col>
                        <Col size={40} style={{paddingTop: 8, paddingRight: 4}}>
                            <Row>
                                <Col style={{paddingTop: 4}}>
                                    <WarningIcon warningType={warningType} />
                                </Col>
                                <Col style={{paddingLeft: 2}}>
                                    <Text
                                        style={{paddingTop: 4, textAlign: 'left'}}
                                        note={!device.muted}>Mute</Text>
                                </Col>
                                <Col style={{paddingLeft: 4}}><BatteryLevel batteryLevel={device.batteryLevel} /></Col>
                            </Row>
                        </Col>
                    </Row>
                </Grid>
                </View>
                </TouchableOpacity>
            </Card>
        );
    }
}