import * as React from 'react';
import { observer, inject } from 'mobx-react';
import { List, ListItem, Text, Left, Right, Body, Radio } from 'native-base';
import { Alert } from 'react-native';
import { formatFrequency } from '../util/frequency';
import DeviceStore, { Channel } from '../store/Devices';

interface Props {
    ip: string;
    showBank: number;
    deviceStore?: DeviceStore;
}

@inject('deviceStore')
@observer
export class BankList extends React.Component<Props, {}> {

    requestTune = (chan: Channel) => {
        let device = this.props.deviceStore.byIp(this.props.ip);

        Alert.alert(
            'Re-tune',
            'Are you sure you want to tune to ' + formatFrequency(chan.frequency) + ' ?',
            [
                {text: 'Cancel', style: 'cancel'},
                {text: 'Yes', onPress: () => {
                    device.setPreset(this.props.showBank, chan.channel);
                }}
            ]
        )
    }

    render() {
        let device = this.props.deviceStore.byIp(this.props.ip);
        
        let items = [];
        let bankChannels = device.bankList[this.props.showBank];
        bankChannels.channel.map((chan) => {
            let selected = false;
            if (device.activeBank === this.props.showBank) {
                if (device.activeChannel === chan.channel) {
                    selected = true;
                }
            }
            items.push((
                <ListItem key={chan.channel}>
                    <Text>{chan.channel} - {formatFrequency(chan.frequency)}</Text>
                    <Right>
                        <Radio selected={selected} onPress={() => this.requestTune(chan)} />
                    </Right>
                </ListItem>
            ));
        })

        return items;
    }
}