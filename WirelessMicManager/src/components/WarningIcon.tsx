import * as React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';

export enum WarningType {
    NONE,
    DANGER,
    WARNING
}

const TYPEPROPERTIES = {
    [WarningType.NONE]: {
        icon: 'check-circle',
        colour: 'green'
    },
    [WarningType.DANGER]: {
        icon: 'times-circle',
        colour: 'red'
    },
    [WarningType.WARNING]: {
        icon: 'exclamation-circle',
        colour: 'orange'
    }
}

interface Props {
    warningType: WarningType
}

export default class BatteryLevel extends React.Component<Props> {

    render() {
        let prop = TYPEPROPERTIES[this.props.warningType];

        return (<Icon name={prop.icon} style={{fontSize: 18, color: prop.colour}} />);
    }
}