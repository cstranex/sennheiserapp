import * as React from 'react';
import { WebView } from 'react-native';
import { observer, inject } from 'mobx-react';
import { reaction, computed } from 'mobx';
import DeviceStore from '../store/Devices';
// import Canvas from 'react-native-canvas';
// import Chart from 'chart.js';

interface Props {
    deviceStore?: DeviceStore;
    ip: string;
}

const createTimepoint = (initial: number) => {
    let d = new Date();
    return initial - Math.floor(d.getTime() / 1000);
}

@inject('deviceStore')
@observer
export default class HistoryGraph extends React.Component<Props> {
    webview = null;
    chart = null;

    reactions = [];
    startPoint = 0;

    @computed get deviceProperties() {
        let device = this.props.deviceStore.byIp(this.props.ip);
        return {
            batteryLevel: device.batteryLevel,
            afLevel: device.afLevel,
            rfALevel: device.rfALevel,
            rfBLevel: device.rfBLevel,
            poll: device.lastReceived
        }
    }

    messageHandler(event: any) {
        let data = event.nativeEvent.data;
        console.log(data);
        if (data === 'ready') {

            // We can begin sending data
            let device = this.props.deviceStore.byIp(this.props.ip);
            this.webview.postMessage(JSON.stringify({
                timepoint: 0,
                battery: this.deviceProperties.batteryLevel,
                af: this.deviceProperties.afLevel,
                rfa: this.deviceProperties.rfALevel,
                rfb: this.deviceProperties.rfBLevel
            }));
            this.startPoint = createTimepoint(0);

            reaction(() => this.deviceProperties, (props) => {
                console.log("going to update!");
                this.webview.postMessage(JSON.stringify({
                    timepoint: createTimepoint(this.startPoint),
                    battery: props.batteryLevel,
                    af: props.afLevel,
                    rfa: props.rfALevel,
                    rfb: props.rfBLevel
                }));
            });
        }
    }

    render() {
        return (
            <WebView
                source={{
                    uri: 'file:///android_asset/chart.html', // NB: in prod android doesn't like this
                }}
                style={{
                    marginTop: 5,
                    height: 600,
                    borderColor: 'black',
                    borderWidth: 1,
                    borderStyle: 'solid',
                    width: '100%'
                }}
                ref={(v) => this.webview = v}
                onMessage={(e) => this.messageHandler(e)}
                injectedJavaScript={'DrawChart();'}
                javaScriptEnabled={true}
            />
        );
    }
}
