import Picker from 'react-native-picker';
import { FreqRange } from '../store/Devices';

const FrequencyPicker = (ranges: FreqRange[], stepSize: number, currentFreq: number) => {
    // Generate start to end ranges
    let data = [];
    ranges.forEach((range) => {
        let sf = Math.floor(range.startFreq / 1000);
        let ef = Math.floor(range.endFreq / 1000);
        for (let i=sf; i <= ef; i++) {
            let key = i.toString(10);
            let entry = [];
            // Calculate in between steps
            for (let n=0; n < 999; n += stepSize) {
                if (((i * 1000) + n) > range.endFreq) {
                    break;
                }

                let value = n + '';

                while (value.length < 3) {
                    value = '0' + value;
                }

                entry.push('.' + value);
            }

            data.push({
                [key]: entry
            });
        }
    });

    let currentHigh = Math.floor(currentFreq / 1000);
    let currentLow = currentFreq - (currentHigh * 1000);

    return new Promise((resolve, reject) => {
        Picker.init({
            pickerTitleText: 'Select Frequency',
            pickerConfirmBtnText: 'Tune',
            pickerCancelBtnText: 'Cancel',
            pickerData: data,
            selectedValue: [currentHigh, '.' + currentLow],
            onPickerCancel: () => {
                reject();
            },
            onPickerConfirm: value => {
                let highFreq = parseInt(value[0], 10) * 1000;
                let freq = highFreq + parseInt(value[1].substr(1), 10);   
                resolve(freq);
            },
        } as any);
        Picker.show();
    });
};

export default FrequencyPicker;