import * as React from 'react';
import { View } from 'react-native';
// import Svg, { Rect, Line } from 'react-native-svg';

const GOOD_LEVEL = '#00FF00';
const HIGH_LEVEL = '#FF0000';


interface Props {
    value: number; // 0 to 100
    peak: number; // 0 to 100
    disabled: boolean;
}

export class AudioLevel extends React.Component<Props> {
    render() {

        let fullWidth = 170;
        let fullHeight = 10;

        let colour = GOOD_LEVEL;
        if (this.props.value > 75) {
            colour = HIGH_LEVEL;
        }

        let value = Math.min(fullWidth - 2, this.props.value / 100 * (fullWidth - 2));
        let peak = Math.min(fullWidth - 2, this.props.peak / 100 * (fullWidth - 2));

        let fill = this.props.disabled ? '#AAAAAA' : 'rgba(0, 0, 0, 0.0)';

        return (
            <View
                style={{
                    height: fullHeight,
                    width: fullWidth,
                    backgroundColor: fill,
                    borderColor: "#000000",
                    borderWidth: 1,
                    borderStyle: "solid"
                }}>
                <View style={{
                    height: 8,
                    width: value,
                    backgroundColor: colour,
                    position: 'absolute',
                    left: 0,
                    top: 0
                }}/>
                <View style={{
                    position: 'absolute',
                    left: peak,
                    top: 0,
                    backgroundColor: 'orange',
                    width: 2,
                    height: 8
                }}/>
            </View>
        )
    }
}

interface RfProps {
    a: number;
    b: number;
    level: number;
    active: string;
    disabled: boolean;
}

export class RfLevel extends React.Component<RfProps> {
    render() {
        let fullWidth = 170;
        let fullHeight = 14;

        let colour = GOOD_LEVEL;
        let activeColour = 'blue';
        let inactiveColour = 'grey';

        if (this.props.level < 30) {
            colour = HIGH_LEVEL;
            activeColour = 'white';
            inactiveColour = '#aaaaaa';
        }

        let aLevel = Math.min(fullWidth - 2, this.props.a / 100 * fullWidth);
        let bLevel = Math.min(fullWidth - 2, this.props.b / 100 * fullWidth);
        let cLevel = Math.min(fullWidth - 2, this.props.level / 100 * fullWidth);

        let fill = this.props.disabled ? '#AAAAAA' : 'rgba(0, 0, 0, 0.0)';


        return (
            <View
                style={{
                    height: fullHeight,
                    width: fullWidth,
                    backgroundColor: fill,
                    borderColor: "#000000",
                    borderWidth: 1,
                    borderStyle: "solid"
                }}>
                <View style={{
                    height: 12,
                    width: cLevel,
                    backgroundColor: colour,
                    position: 'absolute',
                    left: 0,
                    top: 0
                }}/>
                <View style={{
                    position: 'absolute',
                    left: 0,
                    top: 4,
                    backgroundColor: this.props.active === 'a' ? activeColour : inactiveColour,
                    width: aLevel,
                    height: 1
                }}/>
                <View style={{
                    position: 'absolute',
                    left: 0,
                    top: 8,
                    backgroundColor: this.props.active === 'b' ? activeColour : inactiveColour,
                    width: bLevel,
                    height: 1
                }}/>
            </View>
        )
    }
}