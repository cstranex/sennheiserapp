import * as React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';

interface Props {
    batteryLevel: number;
}

export default class BatteryLevel extends React.Component<Props> {

    render() {
        let batteryCol = '#AAAAAA';
        let level = 'empty';
        if (this.props.batteryLevel === -1) {
            batteryCol = '#AAAAAA';
            level = 'empty';
        } else if (this.props.batteryLevel < 30) { // 0 - 29%
            batteryCol = 'red';
            level = '0';
        } else if (this.props.batteryLevel < 70) { // 30 - 69%
            batteryCol = 'orange';
            level = '1';
        } else if (this.props.batteryLevel < 100) { // 70% - 100%
            batteryCol = 'green';
            level = '3';
        } else if (this.props.batteryLevel === 100) {
            batteryCol = 'green';
            level = '4';
        }

        return (<Icon name={'battery-' + level} style={{fontSize: 24, color: batteryCol}} />);
    }
}