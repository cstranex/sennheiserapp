import Picker from 'react-native-picker';

export const SQUELCH = [0, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25];
export const EM500_AFOUT = [-24, -18, -16, -14, -12, -9, 6, 3, 0, 3, 6, 9, 12, 14, 16, 18, 24];
export const EM2000_AFOUT = [];

for (let i=-24; i <= 18; i++) {
    EM2000_AFOUT.push(i);
}
EM2000_AFOUT.push(24);

const DecibelPicker = (range: number[], value: number, title: string) => {

    let data = range.map((n) => (n + ' dB'));

    return new Promise((resolve, reject) => {
        console.log(value + ' dB');
        Picker.init({
            pickerTitleText: title,
            pickerConfirmBtnText: 'Select',
            pickerCancelBtnText: 'Cancel',
            pickerData: data,
            selectedValue: [value + ' dB'],
            onPickerCancel: () => {
                reject();
            },
            onPickerConfirm: selected => {
                resolve(parseInt(selected[0].replace(' dB', ''), 10));
            },
        } as any);
        Picker.show();
    });
}

export default DecibelPicker;