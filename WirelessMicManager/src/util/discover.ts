/* Zeroconf Store

import { PORT } from '../store/Devices';
import zconf from 'react-native-zeroconf';

const zeroconf = new zconf();

interface Service {
    host: string;
    addresses: Array<string>;
    name: string;
    fullName: string;
    port: number;
}

class Discovery {
    constructor() {
        zeroconf.on('stop', () => zeroconf.removeDeviceListeners());
        zeroconf.scan('sennheiser', 'udp', '');
    }

    close() {
        zeroconf.stop();
    }

    callOnPossibleAddresses(callback) {
        console.log(zeroconf.getServices());
        let services = zeroconf.getServices();
        for (let name in services) {
            let service = services[name];
            for (let address of service.addresses) {
                if (service.port !== PORT) {
                    return;
                }
                if (callback(address)) {
                    break;
                }
            }
        }
    }
}

export default new Discovery();
*/