export const NUM_PRESET_BANKS = 20;
export const NUM_USER_BANKS = 6; // On the EM500 there is only one
export const TOTAL_BANKS = NUM_PRESET_BANKS + NUM_USER_BANKS;
export const BANK_ARRAY = [];

export const bankName = (bank: number) : string => {
    if (bank > NUM_PRESET_BANKS) {
        return 'User ' + (bank - NUM_PRESET_BANKS);
    }
    return 'Bank ' + bank;
}

export const formatBank = (bank: number) => {
    // Return a string representation of the bank
    let bankNumber = bank + '';
    let bankName = 'B';
    if (bank > NUM_PRESET_BANKS) {
        bankNumber = (bank - NUM_PRESET_BANKS) + '';
        bankName = 'U0';
    } else {
        if (bank < 9) {
            bankNumber = '0' + bankNumber;
        }
    }

    return bankName + bankNumber;
};

export const formatPreset = (bank: number, channel: number) : string => {
    let bankName = formatBank(bank);
    let ch = '';
    if (channel < 9) {
        ch = '0' + channel;
    } else {
        ch = '' + channel;
    }
    return bankName + '.' + ch;
};

export const formatFrequency = (freq: number) : string => {
    // Return a string formatted version of the frequency
    // eg: 822000 == 822.000 MHz
    let mhz = freq / 1000;
    return mhz.toFixed(3) + ' MHz';
};

for (let i=0; i < TOTAL_BANKS; i++) {
    BANK_ARRAY.push(bankName(i));
}