export enum DeviceModel {
    UNKNOWN,
    EM500,
    EM2000
}

export enum DeviceType {
    UNKNOWN,
    EM_RECEIVER,
    SR_TRANSMITTER
}

export const channelsForDevice = (type: DeviceModel) : number => {
    switch(type) {
        case DeviceModel.UNKNOWN:
            return 0;
        
        case DeviceModel.EM500:
            return 32;
        
        case DeviceModel.EM2000:
            return 64;
    }
}