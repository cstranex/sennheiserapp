import { observable, action, computed } from 'mobx';
import udp from 'react-native-udp';
import { NUM_PRESET_BANKS, NUM_USER_BANKS  } from '../util/frequency';
import { DeviceType, DeviceModel } from '../util/device';
export { DeviceType, DeviceModel } from '../util/device';
import { NetworkInfo } from 'react-native-network-info';

/*
    TODO: Add a background timer for Android (and later iOS) that will call
    the Push message, etc. Remove setInterval and setTimeout and rather use
    timeouts after animations?

    Clean up the _reconnect, _connect, etc.

    Profile and Speed up receiving data from sockets

    Determine the ip address of the device (see broadcast address code)

    Determine if connected to Wi-Fi (or Ethernet for Android). If not, then don't try
    and use the socket.
*/

export const PORT = 53212;

export enum EmEqualizer {
    FLAT,
    LOW_CUT,
    LOW_CUT_HIGH_BOOST,
    HIGH_BOOST
}

export interface SrEqualizer {
    enabled: boolean;

    // Range -5 to 5
    low: number;
    lowMid: number;
    mid: number;
    midHigh: number;
    high: number;
}

export interface Warnings {
    lowBattery: boolean;
    afPeak: boolean;
    lowRf: boolean;
    rfMute: boolean;
    txMute: boolean;
    rxMute: boolean;
}

export interface Channel {
    channel: number; // 1 to 64
    frequency: number; // In Hz
}

export interface Bank {
    bank: number;
    channel: Channel[]; // Each index corresponds to a preset number, value is frequency
}

export interface FreqRange {
    startFreq: number; // In KHz
    endFreq: number; // In KHz
}

const populateBankList = (): Bank[] => {
    let bankList = [];

    // Populate the banks
    for (let i = 0; i < NUM_PRESET_BANKS + NUM_USER_BANKS; i++) {
        bankList.push({
            bank: i,
            channel: []
        });
    }

    return bankList;
};

export class Device {
    timeout: number; // Timeout when no response
    pollInterval: number; // Resend Push message

    socket: any;
    ipAddress: string;

    testingType: boolean; // Testing if it is an EM or SR receiver.

    @observable deviceConnected = false;
    @observable deviceType = DeviceType.UNKNOWN;
    @observable deviceModel = DeviceModel.UNKNOWN;

    /* Device Data (EM Receivers) */
    @observable rfALevel: number;
    @observable rfBLevel: number;
    @observable rfLevel: number; // Apparent RF level
    @observable rfActiveAntenna: 'a' | 'b';
    @observable afMuted: boolean; // AF Muted on transmitter
    @observable rfMuted: boolean; // No signal / RF Muted on transmitted
    @observable batteryLevel: number;
    @observable afOut: number;
    @observable squelch: number;
    @observable pilot: boolean;

    /* Device Data (SR Receivers) */
    @observable stereo: boolean; // If stereo mode
    @observable sensitivity: number;
    @observable afLevel2: number;
    @observable afPeakLevel2: number;

    /* Device Data (All) */
    // @observable muted: boolean; // Receiver (EM) or Transmitter (SR) is muted
    @observable muted: boolean; // Output muted (Audio Out for EM and RF Out for SR)
    @observable afLevel: number;
    @observable afPeakLevel: number;
    @observable equalizer: SrEqualizer | EmEqualizer;
    @observable firmware: string;

    @observable frequency: number = 0; // In KHz
    @observable bankList: Bank[] = populateBankList();
    @observable activeBank: number = 0;
    @observable activeChannel: number = 0;

    @observable name: string;
    @observable freqRange: FreqRange[] = [];

    /*
    @observable minFreq: number; // In KHz
    @observable maxFreq: number; // In KHz
    */

    @observable freqStep: number; // In KHz    
    @observable configIndex: number;

    @observable warnings: Warnings = {
        lowBattery: false,
        afPeak: false,
        lowRf: false,
        rfMute: false,
        txMute: false,
        rxMute: false
    };

    @computed get hasWarning() {
        return this.warnings.lowBattery || this.warnings.afPeak || this.warnings.lowRf || this.warnings.rfMute || this.warnings.txMute || this.warnings.rxMute;
    }

    @observable error: string; // Not yet implemented

    @observable lastReceived: Date;

    setFrequency(freq: number) {
        this._send('Frequency ' + freq + ' 1 1');
    }

    setPreset(bank: number, channel: number) {
        // Lookup bank/channel
        for (let chan of this.bankList[bank].channel) {
            if (chan.channel === channel) {
                this._send('Frequency ' + chan.frequency + ' ' + bank + ' ' + channel);
            }
        }
    }

    getFrequency() {
        this._send('Frequency');
    }

    getName() {
        this._send('Name');
    }

    setName(name: string) {
        this._send('Name ' + name);
    }

    setMute(muted: boolean) {
        this._send('Mute ' + (muted ? '1' : '0'));
    }

    getFirmware() {
        this._send('FirmwareRevision');
    }

    setSquelch(value: number) {
        if (this.deviceType === DeviceType.SR_TRANSMITTER) {
            console.error('Command setSquelch is for EM only');
            return;
        }
        if (value !== 0) {
            if (value < 5) {
                value = 5;
            } else if (value > 25) {
                value = 25;
            } else if (value % 2 !== 0 && value !== 25) {
                // Round down
                value = value - 1;
            }
        }
        this._send('Squelch ' + value);
    }

    getSquelch() {
        if (this.deviceType === DeviceType.SR_TRANSMITTER) {
            console.error('Command getSquelch is for EM only');
            return;
        }
        this._send('Squelch');
    }

    setAfOut(value: number) {
        if (this.deviceType === DeviceType.SR_TRANSMITTER) {
            console.error('Command setAfOut is for EM only');
            return;
        }
        // TODO: Check values
        if (value < -24) {
            value = -24;
        } else if (value > 18) {
            value = 18;
        }
        this._send('AfOut ' + value);
    }

    getAfOut() {
        if (this.deviceType === DeviceType.SR_TRANSMITTER) {
            console.error('Command getAfOut is for EM only');
            return;
        }
        this._send('AfOut');
    }

    setEqualizer(value: EmEqualizer | SrEqualizer) {
        if (this.deviceType === DeviceType.EM_RECEIVER) {
            this._send('Equalizer ' + value);
        } else if (this.deviceType === DeviceType.SR_TRANSMITTER) {
            value = value as SrEqualizer;
            let enabled = value.enabled ? '1' : '0';
            let eq = ' ';
            eq = eq + value.low + ' ' + ' ' + value.lowMid + ' ';
            eq = eq + value.mid + ' ' + value.midHigh + ' ' + value.high;
            this._send('Equalizer ' + enabled + eq);
        }
    }

    setSensitivity(value: number) {
        if (this.deviceType === DeviceType.EM_RECEIVER) {
            console.error('Command setSensitivity is for SR only');
            return;
        }
        if (value < -42) {
            value = -42;
        } else if (value > 0) {
            value = 0;
        }
        this._send('Sensitivity ' + value);
    }

    getSensitivity() {
        if (this.deviceType === DeviceType.EM_RECEIVER) {
            console.error('Command getSensitivity is for SR only');
            return;
        }
        this._send('Sensitivity');
    }

    setMode(stereo: boolean) {
        if (this.deviceType === DeviceType.EM_RECEIVER) {
            console.error('Command setMode is for SR only');
            return;
        }
        this._send('Mode ' + (stereo ? '1' : '0'));
    }

    getMode() {
        if (this.deviceType === DeviceType.EM_RECEIVER) {
            console.error('Command getMode is for SR only');
            return;
        }
        this._send('Mode');
    }

    requestConfiguration() { // Config
        // Request configuration data. NB: Doesn't ask for BankList
        this._send('Frequency\rRfConfig\rName');

        let sbuf = '';
        if (this.deviceType === DeviceType.EM_RECEIVER) {
            sbuf = 'Squelch\rAfOut\rEqualizer';
        } else if (this.deviceType === DeviceType.SR_TRANSMITTER) {
            sbuf = 'Sensitivity\rEqualizer\Mode';
        }

        this._send(sbuf);
    }

    @action.bound
    requestBankList(bank: number) {
        this._send('BankList ' + bank + '\r');
    }

    constructor(ipAddress: string, socket: any) {
        this.ipAddress = ipAddress;
        this.name = '';
        this.batteryLevel = -1;
        this.socket = socket;
    }

    _send(buf: string) {
        if (!buf.endsWith('\r')) {
            buf = buf + '\r';
        }

        var arr = new Uint8Array(buf.length);
        for(let i = 0; i < buf.length; i++) {
            arr[i] = buf.charCodeAt(i);
        }

        clearTimeout(this.timeout);

        this.socket.send(arr, 0, arr.length, PORT, this.ipAddress, () => {
            console.log("Sent!");
            //this.timeout = setTimeout(this._timeout, 10000);
        });
    }

    @action.bound
    _timeout() {
        console.log(["Socket timeout", this.ipAddress]);
        this.deviceConnected = false;
        //setTimeout(() => { this._reconnect() }, 1000);
    }

    @action.bound
    connect() {
        if (!this.deviceConnected) {
            this._reconnect();
        }
    }

    @action.bound
    _reconnect() {
        console.log("_reconnect");
        if (this.deviceConnected) {
            console.log("Trying to reconnect when device connected?");
            return;
        }
        this._socketListen();
    }

    @action.bound
    _socketListen() {
        /* Send this message every 50 seconds to ensure we keep receiving information
        from the device. */
        clearInterval(this.pollInterval);
        this.pollInterval = setInterval(this._socketListen, 50000);

        let buf = 'Push 60 100 7\r';
        if (this.deviceType === DeviceType.UNKNOWN) {
            // If we don't know what type of device this is, send an AfOut get request
            // If it is an SR transmitter it will fail
            buf += 'AfOut\rMode';
            this.testingType = true;
        }
        this._send(buf);
    }

    @action.bound
    _socketReceive(message: string) {
        // Receive data from the device

        this.lastReceived = new Date();

        if (!this.deviceConnected) {
            this.deviceConnected = true;
        }
        clearTimeout(this.timeout);

        let data = message.split(' ');

        switch (data[0]) {
            case 'Push':
                // Acknowledge the push message
                break;

            case 'Msg': // Warning messages
                this.warnings.rxMute = message.indexOf('RX_Mute') !== -1;
                this.warnings.lowBattery = message.indexOf('Low_Battery') !== -1;
                this.warnings.afPeak = message.indexOf('AF_Peak') !== -1;
                this.warnings.lowRf = message.indexOf('Low_RF_Signal') !== -1;
                this.warnings.rfMute = message.indexOf('RF_Mute') !== -1;
                this.warnings.txMute = message.indexOf('TX_Mute') !== -1;
                break;

            case 'Squelch':
                this.squelch = parseInt(data[1], 10);
                break;

            case 'AfOut':
                this.afOut = parseInt(data[1], 10);
                break;

            case 'Equalizer':
                if (this.deviceType === DeviceType.EM_RECEIVER) {
                    this.equalizer = parseInt(data[1], 10);
                } else if (this.deviceType === DeviceType.SR_TRANSMITTER) {
                    this.equalizer = {
                        enabled: data[1] === '1',
                        low: parseInt(data[2], 10),
                        lowMid: parseInt(data[3], 10),
                        mid: parseInt(data[4], 10),
                        midHigh: parseInt(data[5], 10),
                        high: parseInt(data[6], 10)
                    };
                }
                break;

            case 'RF1': // Ant A
                this.rfALevel = parseInt(data[1], 10);
                break;

            case 'RF2': // Ant B
                this.rfBLevel = parseInt(data[1], 10);
                break;

            case 'RF': // Overall level and also which antenna is active
                this.rfLevel = parseInt(data[1], 10);
                this.rfActiveAntenna = data[2] === '1' ? 'a' : 'b';
                this.pilot = data[3] === '1';
                break;

            case 'AF': // AF level
                this.afLevel = parseInt(data[1], 10);
                this.afPeakLevel = parseInt(data[2], 10);
                if (this.deviceType === DeviceType.SR_TRANSMITTER) {
                    this.afLevel2 = parseInt(data[3], 10);
                    this.afPeakLevel2 = parseInt(data[4], 10);
                }
                break;

            case 'Bat': // Battery level
                this.batteryLevel = data[1] === '?' ? -1 : parseInt(data[1], 10);
                break;

            case 'Sensitivity':
                this.sensitivity = parseInt(data[1], 10);
                break;

            case 'Mode':
                this.stereo = data[1] === '1';
                break;

            case 'Config': // Config info
                let index = parseInt(data[1], 10);
                if (this.configIndex !== index) {
                    this.configIndex = index;
                    // Also request configuration data
                    this.requestConfiguration();
                }
                break;

            case 'States':
                if (this.deviceType === DeviceType.EM_RECEIVER) {
                    let flags = parseInt(data[1], 10);
                    this.afMuted = (flags & 2) !== 0; // eslint-disable-line no-bitwise
                    this.rfMuted = (flags & 4) !== 0; // eslint-disable-line no-bitwise
                } else if (this.deviceType === DeviceType.SR_TRANSMITTER) {
                    this.muted = data[1] === '1';
                }
                break;

            case 'BankList':
                this.bankList[data[1]].channel = [];
                for (let i = 2; i < data.length; i++) {
                    if (data[i] === '0' || data[i] === '') {
                        continue;
                    }
                    this.bankList[data[1]].channel.push({
                        frequency: parseInt(data[i], 10),
                        channel: i - 1
                    });
                }
                if (data.length > 35) {
                    this.deviceModel = DeviceModel.EM2000;
                } else {
                    this.deviceModel = DeviceModel.EM500;
                }
                break;

            case 'Frequency':
                this.frequency = parseInt(data[1], 10);
                this.activeBank = parseInt(data[2], 10);
                this.activeChannel = parseInt(data[3], 10);
                break;

            case 'RfConfig':
                this.freqRange.splice(0, this.freqRange.length);
                for (let i=1; i < data.length; i+=3) {
                    let range = {
                        startFreq: parseInt(data[i], 10),
                        endFreq: parseInt(data[i + 1], 10)
                    }
                    this.freqRange.push(range);
                }
                this.freqStep = parseInt(data[3], 10);
                break;

            case 'Name':
                this.name = data.slice(1).join(' ').trim();
                break;

            case 'Mute':
                this.muted = data[1] === '1';
                break;

            case 'FirmwareRevision':
                this.firmware = data[1];
                break;

            case '1000:':
                // If testing for a type then catch this
                if (this.testingType === true) {
                    if (message.search('Mode') !== -1) {
                        this.deviceType = DeviceType.EM_RECEIVER;
                    } else if (message.search('AfOut') !== -1) {
                        this.deviceType = DeviceType.SR_TRANSMITTER;
                    }
                    this.testingType = false;
                }
                break;

            default:
        }
    }

    @action.bound
    _socketError(data: any) {
        this.deviceConnected = false;
        console.log(["Socket error", data]);
    }
}

export default class DeviceStore {
    @observable devices: Device[] = [];

    socket: any;
    deviceIp: string;

    @observable discovered: Map<string, string> = new Map();

    @computed get deviceByIp() {
        const deviceByIp = {};

        this.devices.map((d) => {
            deviceByIp[d.ipAddress] = d;
        });

        return deviceByIp;
    }

    constructor() {
        this.socket = udp.createSocket({
            reusePort: true,
            type: 'udp4'
        });
        // this.socket.on('listening', () => this.onListen);
        this.socket.on('message', (payload, rinfo) => this.onSocketData(payload, rinfo));
        this.socket.on('error', (err) => {
            console.log(["Error", err]);
        })
        this.socket.bind(PORT, () => this.onListen());

        NetworkInfo.getIPV4Address((address: string) => {
            this.deviceIp = address;
        });
    }

    onListen() {
        //this.socket.setBroadcast(true);
    }

    @action.bound
    onSocketData(payload: Uint8Array, rinfo) {
        const msg = String.fromCharCode.apply(null, payload);

        let device = this.deviceByIp[rinfo.address];
        if (device) {
            msg.split('\r').forEach((message) => device._socketReceive(message));
        } else {
            if (this.deviceIp === rinfo.address) {
                return;
            }
            let msgs = msg.split('\r');
            for (let msg of msgs) {
                if (/^Name /.test(msg)) {
                    /*
                    if (!this.discovered.has(rinfo.address)) {
                        this.discovered.set(rinfo.address, "New Device");
                    }
                    */
                    console.log("Discovered device has a name " + msg);
                    this.discovered.set(rinfo.address, msg.substr('Name '.length).trim());
                    return;
                }
            }
        }
    }

    discoverDevices(broadcastAddress: string) {
        /* Send a broadcast message to all devices in the hopes of receiving
        a response */

        NetworkInfo.getIPV4Address((address: string) => {
            this.deviceIp = address;
        });

        let localSock = udp.createSocket({
            reusePort: true,
            type: 'udp4'
        });
        localSock.on('error', () => {
            // localSock.close();
        })

        let buf = "Name\r";
        var arr = new Uint8Array(buf.length);
        for(let i = 0; i < buf.length; i++) {
            arr[i] = buf.charCodeAt(i);
        }
        localSock.send(arr, 0, arr.length, PORT, broadcastAddress, () => {
            localSock.close();
        });

    }

    byIp(ipAddress: string): Device {
        // Todo: Remove me
        return this.deviceByIp[ipAddress];
    }

    findDevice(ipAddress: string): boolean {
        return this.deviceByIp[ipAddress] !== undefined;
    }

    @action.bound
    addDevice(ipAddress: string): Device {

        if (this.findDevice(ipAddress)) {
            return null;
        }
        
        if (this.discovered.has(ipAddress)) {
            this.discovered.delete(ipAddress);
        }

        let device = new Device(ipAddress, this.socket);

        this.devices.push(device);
        return device;
    };
}
