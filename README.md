This is an unfinished React-Native app written in Typescript that was designed
to work with Sennheiser wireless microphones and provide stats and information
on their properties in real time for an operator.

For various reasons I was unable to complete the app so I am releasing the code
in case it helps anyone. In the future I may revisit these but since the G4
series of receivers do have an app, it is unlikely I'll complete the project.

The app itself worked and was able to give information such as RF, AF and
battery level as well as frequency information, name information, etc.

The original goal was to use the app during set-up at events to aid in antenna
placement and during sound checks to monitor peaking.